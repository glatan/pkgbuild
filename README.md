# PKGBUILD

## pacman.conf

```text
[glarch]
SigLevel = Never
Server = https://gitlab.com/glatan/pkgbuild/-/raw/master/release/
Server = https://pkgbuild.glatan.vercel.app/
```

## Packages

|Name|Version|
|-|-|
|mint|0.22.0-1|
|numix-icon-theme|25.01.31-1|
|numix-icon-theme-circle|25.01.31-1|
|paru|2.0.4-1|
|xkcp-git|r416.6fa655a-1|

## Makefile

%: pkgname

### p.build

Build Podman container.

### %.build

Build package.

### %.new

Init new package.

### %.updatepkgsums

Update checksums.

### check.update

Check repository-source update.

### clean

Remove untracked files.

### run.bash

Run GNU bash on Podman container.
